
/***************************************************

1. !!!Make sure you using lastest ESP8266 core for Arduino, otherwise it may not work properly.
  https://github.com/esp8266/Arduino
  
  (The pin D0(GPIO16) will be not work when you use older version ESP8266 core for Arduino, 
  because the older version ESP8266 core for Arduino's digitalPinToBitMask(), portOutputRegister(), 
  portInputRegister() and portModeRegister() fuction have some bugs which Adafruit_ILI9341 Library will use.
  This bug was fixed after commit:  https://github.com/esp8266/Arduino/commit/799193888a553de8876052019842538396f92194 )
    

2. Setup latest Adafruit_GFX, Adafruit_ILI9341 and XPT2046_Touchscreen Library first:

    https://github.com/adafruit/Adafruit-GFX-Library

    https://github.com/adafruit/Adafruit_ILI9341

    https://github.com/PaulStoffregen/XPT2046_Touchscreen

****************************************************/
#include <ESP8266WiFi.h>
#include <ESP8266HTTPClient.h>
#include <ArduinoJson.h>


#include <SPI.h>
#include <Adafruit_GFX.h>
#include <Adafruit_ILI9341.h>

#define TFT_CS D0  //for D1 mini or TFT I2C Connector Shield (V1.1.0 or later)
#define TFT_DC D8  //for D1 mini or TFT I2C Connector Shield (V1.1.0 or later)
#define TFT_RST -1 //for D1 mini or TFT I2C Connector Shield (V1.1.0 or later)
#define TS_CS D3   //for D1 mini or TFT I2C Connector Shield (V1.1.0 or later)

// #define TFT_CS 14  //for D32 Pro
// #define TFT_DC 27  //for D32 Pro
// #define TFT_RST 33 //for D32 Pro
// #define TS_CS  12 //for D32 Pro

Adafruit_ILI9341 tft = Adafruit_ILI9341(TFT_CS, TFT_DC, TFT_RST);

int vertical = 15;


//=== variables ===//
const char* user_name = "N/A";
const char* RankInfo = "N/A";
const char* Matches_Played = "N/A";
const char* Score = "N/A";
const char* KD = "N/A";
const char* Kills = "N/A";
const char* Wins = "N/A";
//=== variables ===//


// WiFi Parameters
const char* ssid = "Majutek_2.4G";
const char* password = "Czekolada01";


void setup()
{
  Serial.begin(115200);
  WiFi.begin(ssid, password);

tft.begin();
//tft.setRotation(1)


tft.fillScreen(ILI9341_BLACK);
tft.setCursor(10, vertical+10);
tft.setTextColor(ILI9341_RED);
tft.setTextSize(2);
tft.println("Connecting");
tft.setCursor(130, vertical+10);
 
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    tft.print(".");
  }
  
  setTemplate();
}

void loop(void)
{

  
 // Check WiFi Status
  if (WiFi.status() == WL_CONNECTED) {
    HTTPClient http;  //Object of class HTTPClient
    //http.begin("http://jsonplaceholder.typicode.com/users/1");

    http.begin("http://api.fortnitetracker.com/v1/profile/pc/to_wina_tuska");
      http.addHeader("TRN-Api-Key", "58558e89-294f-4fb7-91bd-e9cdca63a8c7");
    
    int httpCode = http.GET();
    //Check the returning code                                                                  
    if (httpCode > 0) {

      
//      // Parsing
      const size_t bufferSize = JSON_OBJECT_SIZE(2) + JSON_OBJECT_SIZE(3) + JSON_OBJECT_SIZE(5) + JSON_OBJECT_SIZE(8) + 370;
      DynamicJsonBuffer jsonBuffer(bufferSize);
      JsonObject& root = jsonBuffer.parseObject(http.getString());
                  
      // Parameters
      int id = root["id"]; // 1
      user_name = root["epicUserHandle"];                 
      RankInfo = root["stats"]["p2"]["trnRating"]["rank"];

      JsonArray& lifeTimeStats = root["lifeTimeStats"];
      Matches_Played = "noValue";
      Score = "noValue";
      KD = "noValue";
      Kills = "noValue";
      Wins = "noValue";

      for (int i = 0; i < 12; i++) {
          if (lifeTimeStats[i]["key"] == "Matches Played"){
            Matches_Played = lifeTimeStats[i]["value"];
          }
          if (lifeTimeStats[i]["key"] == "Score"){
            Score = lifeTimeStats[i]["value"];
          }
          if (lifeTimeStats[i]["key"] == "K/d"){
            KD = lifeTimeStats[i]["value"];
          }
          if (lifeTimeStats[i]["key"] == "Kills"){
            Kills = lifeTimeStats[i]["value"];
          }
          if (lifeTimeStats[i]["key"] == "Wins"){
            Wins = lifeTimeStats[i]["value"];
          }
        }      

      // Output to serial monitor
      Serial.print("Name:");
      Serial.println(user_name);
      
      Serial.print("Rank:");
      Serial.println(RankInfo);
      
      Serial.print("Score:");
      Serial.println(Score);

      Serial.print("KD:");
      Serial.println(KD);

      Serial.print("Kills:");
      Serial.println(Kills);

      Serial.print("Wins:");
      Serial.println(Wins);
      
    }
    http.end();   //Close connection
  }
  // Delay
  setTemplate();
  delay(60000);
}

void setTemplate(){
  tft.fillScreen(ILI9341_BLACK);
  tft.setCursor(40, vertical+10);
  tft.setTextColor(ILI9341_RED);
  tft.setTextSize(2);
  tft.println(user_name); 

  tft.setCursor(30, vertical+40);
  tft.setTextColor(ILI9341_RED);
  tft.setTextSize(2);
  tft.println("SCORE");
   
  
  tft.drawRoundRect(0, vertical+60, 110, 60, 10, ILI9341_WHITE);  // Draw rectangle (x,y,width,height,color)
  tft.setCursor(10, vertical+80);  tft.setTextColor(ILI9341_YELLOW);  tft.setTextSize(2);  tft.println(Score);
  
                                            
  tft.setCursor(160, vertical+40);
  tft.setTextColor(ILI9341_RED);
  tft.setTextSize(2);
  tft.println("RANK");
                                           
  
tft.drawRoundRect(129, vertical+60, 110, 60, 10, ILI9341_WHITE);  // Draw rounded rectangle (x,y,width,height,radius,color)
tft.setCursor(140, vertical+80);  tft.setTextColor(ILI9341_YELLOW);  tft.setTextSize(2);  tft.println(RankInfo);                                                     // It draws from the location to down-right

tft.setCursor(20, vertical+130);  tft.setTextColor(ILI9341_RED);  tft.setTextSize(2);  tft.println("WINS"); 
tft.drawCircle(40, vertical+180, 30, 0x001F);  // Draw circle (x,y,radius,color)
tft.setCursor(30, vertical+170);  tft.setTextColor(ILI9341_YELLOW);  tft.setTextSize(2);  tft.println(Wins);

tft.setCursor(97, vertical+130);  tft.setTextColor(ILI9341_RED);  tft.setTextSize(2);  tft.println("KILS"); 
tft.drawCircle(120, vertical+180, 30, 0x001F);  // Draw circle (x,y,radius,color)
tft.setCursor(105, vertical+170);  tft.setTextColor(ILI9341_YELLOW);  tft.setTextSize(2);  tft.println(Kills);


tft.setCursor(170, vertical+130);  tft.setTextColor(ILI9341_RED);  tft.setTextSize(2);  tft.println("MATCH");
tft.drawCircle(200, vertical+180, 30, 0x001F);  // Draw circle (x,y,radius,color)
tft.setCursor(185, vertical+170);  tft.setTextColor(ILI9341_YELLOW);  tft.setTextSize(2);  tft.println(Matches_Played);

tft.setCursor(110, vertical+220);  tft.setTextColor(ILI9341_RED);  tft.setTextSize(2);  tft.println("KD"); 
tft.drawCircle(120, vertical+270, 30, 0x001F);  // Draw circle (x,y,radius,color)
tft.setCursor(96, vertical+260);  tft.setTextColor(ILI9341_YELLOW);  tft.setTextSize(2);  tft.println(KD);


// Draw line:
//tft.drawLine(0, 230, 239, 230, 0x07FF);  // Draw line (x0,y0,x1,y1,color)
//  Draw circle:
//tft.drawCircle(30, 289, 30, 0x07E0);  //  Draw circle (x,y,radius,color)
  }
